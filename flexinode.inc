<?php
/**
 *
 * Metadata extractors for flexinode
 *
 * This file bridges flexinode and query_export
 *
 */

  /**
   General nodes about approach here. For now, let's try
   the simplest thing that will work and will be compatible
   with the corresponding view of CiviCRM contact data.  We
   need a description of a type,and a description of a field.

   We will load these functions as needed, and wrap them
   so that most of these functions will effectively private.

   Naming of public functions should end "_flexinode_type"

  **/


/**
 * 
 * Getter for a flexinode data type.
 * 
 * @param $desc_id a descriptor for a Flexinode data type.  An int.
 * @return array meta data object 
 */
function query_export_get_flexinode_type($desc_id){
  //desc id for this type is really a cid
  $sql = "SELECT * FROM {flexinode_type} WHERE ctype_id = %d";
  $tobj = db_fetch_object(db_query($sql, $desc_id));
  if (!$tobj) {
    //TO DO: Consider sending something to watchdog here
    return NULL;
  }

  //The type exists, so build an object
  $meta_data = array(
		     'engine' => 'flexinode', //what we need to call
		     'name' => "flexinode-$desc_id", //bogus name
		     'title' => $tobj->name,
		     'description' => $tobj->description,
		     'fields' => query_export_get_flexinode_fields($desc_id) 
		     );
  return $meta_data;
  
}


/**
 * Record getter
 *
 */
function query_export_get_records_flexinode_type($meta_data, $qname, $args, $start, $num_recs){
  //TO DO: integrate real queries
  //These are just nodes, and our ids in this case are NIDs.
  $list = array();    
  $fields = $meta_data['fields'];
  $type = $meta_data['name']; //name in this case is the drupal type

  //Let's get all records for the type
  $sql = "SELECT nid FROM {node} WHERE type = '%s' ORDER BY nid";
  $rslt = db_query($sql, $type);
  if(!$rslt)
    return array();
  while($row = db_fetch_array($rslt)){
    $nid = $row['nid'];
    $node = node_load($nid); //4.7 syntax.
    //Do we need to check for access?
    if($node and $node->type == $type){
      //I walk the line...
      //First, add a couple of standard fields
      $record = array(
                      'title' => $node->title,
                      'nid' => $node->nid
                     );
      foreach($fields as $field){
	$fname = $field['name'];
	$record[$fname] = $node->$fname;
      }
      $list[] = $record;
    }

  }
  return $list;
}



/**
 * Private getters
 *
 */

/**
 * This translates flexinode metadata for fields into
 * a standard format.
 */

function query_export_get_flexinode_fields($cid){
  //Our desc is really a cid, so get our fields in display
  //order.
  $sql = "SELECT * FROM {flexinode_field} WHERE ctype_id = %d ORDER BY weight";
  $rslt = db_query($sql, $cid);
  if (!$rslt) {
    return NULL;
  }

  $list = array();
  while ($row = db_fetch_object($rslt)) {
    $field_def = array();
    //Flexinode fields don't really have variable
    //names, so we will generate something out of
    //cid and fid.
    $fid = $row->field_id;
    $field_def['name'] = "flexinode_$fid"; //in this case, as it appears in node load.
    $field_def['title'] = $row->label; //human readable title
    $field_def['type'] = $row->field_type; //I will accept fn names as canonical
    $field_def['description'] = $row->description;
    
    $list[] = $field_def;
  }

  return $list;
}







?>