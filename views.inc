<?php
/**
 *
 * QueryExport layer for modules supporting the Views API
 *
 * @author Rob Thorne <rob@torenware.com>
 * @copyright  2006 Collective Heritage Institute
 *
 */



function query_export_list_views_queries(){
	//TO DO: we need to do an access test.
	//For now, let's assume so...
	global $user;
	$views_list = array();
	//We need to process default views and user views separately.
	$dviews = _views_get_default_views();
	foreach ($dviews as $vname => $view) {
		//Hopefully we solve this at the cache level, since
		//we don't register a view if you can't see it
		
		//^^^^ Title is now the same as name ^^^^^
		//$title = (isset($view->page_title) and $view->page_title) ?
		//	$view->page_title : $vname;
		$title = $vname;
		$views_list[$vname] = $title;
	}

	//Now query to view_view table.  We need to get
	//our accesses.
	$roles = user_roles();
	$sql = "SELECT name, access, description, page_title FROM {view_view}";
	$rslt = db_query($sql);
	while ($row = db_fetch_array($rslt)) {
		if (!$user->uid == 1 and $row['access']) {
			//this is a little nasty, since current versions of
			//views are stashing %20 chars in this list.  Why,
			//I cannot guess
			$has_access = false;
			foreach (explode(',', $row['access']) as $access) {
				$access = trim($access);
				if (isset($roles[$access])) {
					$has_access = true;
					break;
				}
			}
			if(!$has_access)
				continue;
		}
		$title = $row['name'];
		//if (!$title)
		//	$title = $row['name'];
		$views_list[$row['name']] = $title;
	}
	//And now we have both lists
	return $views_list;
}

/**
 * Type handler for views API views.
 *
 *
 * @author
 * @param string $desc_id  the view name.
 * @return
 * @exeption
 * @see
 */

function query_export_get_views_type($desc_id){
  if (!module_exist('views'))
    return array(); //error, really
	$view = views_get_view($desc_id);
	//TO DO:  we should respect access info
	//as returned by views_get_view.

	//Now begins the fun.  Start processing our
	//info...
	if (!$view)
		return array(); //may want to generate error here.

	$fields = array();
	foreach ($view->field as $field) {
		$fld_def = array('title' => $field['label'],
										 'qname' => $field['queryname'],
										 'name' => $field['queryname'] . "_" . $field['position'],
										 'position' => $field['position'],
										 'type' => 'string', //nothing to do here
										 'description' => 'Field from a Drupal view', //lame but accurate
										 'weight' => $field['weight'] ? $field['weight'] : 0,
										 );
		$fields[] = $fld_def;

	}
	
	$metadata = array('fields' => $fields,
										'engine' => 'views',
										'title' => $view->page_title,
										'description' => $view->description,
										'name' => $desc_id,
										'private' => array('view' => $view) //private data
										);
	return $metadata;  //caching occurs higher up.

}


function query_export_get_views_record_array($meta_data, $qname, $query_nid, 
																						 $args, $start, $num_recs){
	//TO DO: paging code is still not hooked up.

	$view = $meta_data['private']['view'];
	$fields = $meta_data['fields'];
	$views_fields = $view->field;
	$all_fields = _views_get_fields();

	$records = array();
	//We likely need to munge the view object for purposes of paging here.
	//this is an important TO DO.
	$args = explode('/', $args);
	//not sure about "use pager" argument
	$info = views_build_view('items', $view, $args, FALSE, $num_recs);
	//get a look-up for field info.
	$field_lookup = array();
	foreach ($views_fields as $field_def) {
	  //$field_def['fullname'] =
	  $field_lookup[$field_def['queryname'] . "_" . $field_def['position']] = $field_def;
	}
	$result = array();
	//TO DO: might be faster if I separate out field names
	
	//Get formatting info
	foreach ($info['items'] as $item) {
	  $row = array();
	  foreach ($fields as $field) {
	    $fld_name = $field['qname'];
	    $export_name = $field['name'];
	    if (isset($item->$fld_name)) {
	      $value = $item->$fld_name;
	      $field_info = $field_lookup[$export_name];
	      $formatted = views_theme_field('views_handle_field', 
	                                     $fld_name, 
	                                     $all_fields, $field_info, $item);
	      $row[$export_name] = $formatted;
	    }
	  }
		//$result[$start++] = $row;
		if ($row)
			$result[] = $row;
	}

	return $result;
	
}




?>