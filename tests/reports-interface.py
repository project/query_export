#! /usr/bin/python


## Here's a test script to see if our xmlrpc server is doing
## what we want it to do, or if it's just speaking smack.
from pprint import pprint
import xmlrpclib
import stateful_rpc

# Set debug to something non-zero
# if you want to use the Zend PHP debugger.
# If you use a port for Zend other than
# 10000, you need to set this up in your
# Zend development environment
debug = 0
zend_port = 10100


targ = 'http://your-drupal-server/xmlrpc.php'

# Change these for your system. Note that the
# XMLRPC user needs XML Query access until query_export.module

user_name = 'xmlrpc-user'
passwd = 'drupal-password'

## I use the Zend debugger.  Turn on Debug if you want
## debug your XMLRPC server code in Drupal
if debug:
    zend = stateful_rpc.ZendDebugInfo()
    zend.set_debug_port(zend_port)
    rpc = stateful_rpc.ZendServer(targ, zend)
else:
    rpc = xmlrpclib.ServerProxy(targ)

methods = rpc.system.listMethods() 
print "Print out methods exported by our server..."
pprint(methods)
print

print "Get info about the methods..."
print
for method in methods:
    print "----- " + method + " -------"
    rslt = rpc.system.methodHelp(method)
    pprint(rslt)
    print

print

## You'll need to create a user with the appropriate
## user name and password for this to work, of course.
print "Log in and get your credentials:"
auth = rpc.drupal.qexport.login(user_name, passwd)
pprint(auth)

print "tried to log in"

if not auth:
    print "did not authenticate"
else:
    print "Get queries:"
    qparams = {}
    queries = rpc.drupal.qexport.query.list(auth['cookie'], qparams)
    pprint(queries)
    print

    for query in queries:
        print query
        meta = rpc.drupal.qexport.query.metadata(auth['cookie'], query)
        pprint(meta)
        print

        ## Uncomment this if you want lots and lots of output
        #print "Records:"
        #params = {}
        #records = rpc.drupal.qexport.query.run(auth['cookie'], query, params)
        #pprint(records)
        print "\n----------------------"
        print
        
print
print "done"



