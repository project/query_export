<?php
/**
 *
 * Query Export Formatters:  creates some basic formatters for
 * numerical data, and implements a hook for adding more.
 *
 * A formatter is a function with the sig
 *
 *   function my_qe_formatter($var_name, $query_nid, $value, $extra = NULL)
 *
 * It should return a string that will be passed out by XMLRPC to the
 * query engine (currently, Jasper Reports). 
 *
 */


/**
 * Apply a formatter to data
 *
 *
 * @author
 * @param
 * @return
 * @exeption
 * @see
 */
function query_export_munge_field($query_nid, $field_name, $formatter, $value){
	if ($formatter['handler'] and $formatter['handler'] != 'null') {
		//we assume that function_exists was called up stream
		$func = $formatter['handler'];
		return $func($field_name, $query_nid, $value, $formatter['extra']);
	}
	
	//otherwise, just pass it through
	return $value;
}


/**
 * Main API function: this one fetches and caches definitions,
 * and does a bit of sanity checking on the way.
 *
 *
 * @author
 * @param
 * @return
 * @exeption
 * @see
 */
function query_export_fetch_formatter($query_nid, $var_name = NULL, $data = NULL){
	//We negative cache functions we cannot find...
  static $cache;
	if (!$data) {
		//If we don't specify for a specific field,
		//just return all of them for this field.
    if (!$var_name)
			return $cache[$query_nid];

		//Do a look up
		if (!isset($cache[$query_nid][$var_name]))
			return NULL;
		else
			return $cache[$query_nid][$var_name];
	}

	//We are loading the cache
	//Data has keys handler, extra
	if (isset($data['handler'])) {
		$formatter = $data['handler'];
		if (function_exists($formatter)) {
			if (!isset($cache[$query_nid]))
				$cache[$query_nid] = array();
			$cache[$query_nid][$var_name]['handler'] = $formatter;
			if (isset($data['extra']))
				$cache[$query_nid][$var_name]['extra'] = $data['extra'];
		}
		else {
			//to replace with watchdog
			error_log("Ignoring undefined handler $formatter for $query_id::$var_name");
			if (!isset($cache[$query_nid]))
				$cache[$query_nid] = array();
			//negative cache this
			$cache[$query_nid][$var_name] = NULL;
		}
	}
}


/**
 * Return defined fields as meta data
 *
 */
function query_export_get_custom_meta_data($nid){
	//for now, someone needs to call the loader first.
	$cache = query_export_fetch_formatter($nid);
	//$metadata = array('private' => array()); //make sure this isn't empty
	$fields = array();
	foreach ($cache as $field_name => $data) {
		$fields[] = 
			array('name' => $field_name,
						'title' => $field_name, //to do: make this settable??
						'type' => 'string',
						'description' => '');
	}

	$node = node_load($nid);
	list($base, $name) = explode('::', $node->query_name);
	$metadata = array('fields' => $fields,
										'engine' => 'custom',
										'title' => $node->title,
										'description' => '',
										'name' => $name,
										'private' => array() //private data
										);
	return $metadata;

}



/**
 * Load formatter definitions from the db
 *
 */
function query_export_load_field_definitions($nid){
	$sql = "SELECT * FROM {query_export_data_handlers} WHERE nid = %d";
	$rslt = db_query($sql, $nid);
	if (!$rslt)
		return 0;
	$num_rec = db_num_rows($rslt);
	if (!$num_rec)
		return 0;
	//We have 'em, load them now
	while ($row = db_fetch_array($rslt)) {
		query_export_fetch_formatter($nid, $row['field_name'], $row);
	}
	return $num_rec;
}

/**
 * Fetcher for handlers
 *
 */
function query_export_get_data_handlers($handler = NULL, $handler_title = NULL){
	static $cache;
	if (!$cache) {
		//create a null handler, which is a pass through
		$cache['null'] = t('Just Pass Data Through');
	}
	if (!$handler) {
		//return everything, say for a select popup
		query_export_prime_handler_cache();
		return $cache;
	}
	//Or just this handler
	if (!$handler_title)
		return $cache[$handler]; 
	else //we are setting the cache
		$cache[$handler] = $handler_title;
}


/**
 * Prime handler cache
 *
 */
function query_export_prime_handler_cache(){
	static $been_there;
	if ($been_there) {
		return;
	}
	else
		$been_there = 'done that';
	$built_ins =
		array('query_export_format_drupal_nid_handler' =>
					t('Format as a Drupal Node'),
					'query_export_format_crm_contact_handler' =>
					t('Format as CiviCRM contact field'),
					'query_export_format_crm_group_handler' =>
					t('Format as a CiviCRM group name')
					);
	foreach ($built_ins as $handler => $handler_title) {
		query_export_get_data_handlers($handler, $handler_title);
	}
}


function query_export_format_crm_contact_handler($var_name, $query_nid, 
																								 $value, $extra = 'display_name'){
	//May want to do an access check here.
	if (!module_exist('civinode') or !is_numeric($value))
		return $value;
	$data = civinode_util_load_cdata($value);
	if (!$data)
		return $value;
	if (!$extra)
	  $extra = 'display_name';
	if (isset($data[$extra]))
		return $data[$extra];
	else
		return '';  //how best to deal with missing fields?

}



function query_export_format_drupal_nid_handler($var_name, $query_nid, 
																								$value, $extra = 'title'){
	//May want to do an access check here.
	if (!$extra)
	  $extra = 'title';
	if (!is_numeric($value))
		return $value;
	$node = node_load($value);
	if (!$node)
		return $value;
	if (isset($node->$extra))
		return $node->$extra;
	else
		return $value;  //how best to deal with missing fields?

}



function query_export_format_crm_group_handler($var_name, $query_nid, 
																							 $value, $extra = NULL){
	//May want to do an access check here.
	if (!module_exist('civinode') or !is_numeric($value))
		return $value;
	$data = civinode_get_group_by_id($value);
	if (!$data)
		return $value;
	else
		return $data;

}


/**
 * Editor/Deleter form for field data
 *
 */
function query_export_edit_field_info_form($nid, $field_name = NULL){
	$submit_value = t('Submit');
	$delete_value = t('Delete');
	$copy_value = t('Copy');
	
	if (!$nid) 
	  drupal_not_found();
  $qdef = node_load($nid);
  if (!$qdef)
    drupal_not_found();
  //See if we have previous values
  $old = query_export_fetch_field_def($nid, $field_name);
  
	$output = t("<p>Use this form to format your field data in a more readable 
fasion, or to make Jasper Reports aware of the fields in your custom query.  
The default formatter just passes the data in your query directly out
to Jasper Reports, but you can do things like turn various codes into
human readable strings, or to change the format that Jasper sees.
</p>
");
	$handler_options = query_export_get_data_handlers();
	//Stuff to pass to handlers
	$form['qe_values'] = 
		array('nid' =>
					array('#type' => 'value',
								'#value' => $nid));
   $form['general_info'] =
     array('#type' => 'fieldset',
           '#title' => t('For Field'));
   $form['general_info']['which_nid'] =
     array('#type' => 'item',
           '#title' => t('Query Title'),
           '#value' => $qdef->title);
	//We may be called for a new field or to edit an existing one
	if (!$field_name)
		$form['general_info']['field_name'] =
			array('#type' => 'textfield',
						'#title' => t('Field Name'),
						'#description' => t('DB Field to expose or format'));
	else{
		$form['qe_values']['field_name'] =
			array('#type' => 'value',
						'#value' => $field_name);
   $form['general_info']['which_field'] =
     array('#type' => 'item',
           '#title' => t('Field Name'),
           '#value' => $field_name);
		
	}
	$form['formatter_form'] =
		array('handler' =>
					array('#type' => 'select',
								'#options' => $handler_options,
                '#default_value' => $old['handler'] ? $old['handler'] : 'null',
								'#title' => t('Choose A Formatter'),
								'#description' =>
								t('If your data is a bit cryptic, you can choose a formatter to make it easier for a human to understand.'),
								),
					'extra' =>
					array('#type' => 'textfield',
								'#size' => 40,
								'#default_value' => $old['extra'] ? $old['extra'] : NULL,
								'#title' => t('Extra Formatter Data'),
								'#description' =>
								t('Some formatters can take a bit of additional information.  For example, a CiviCRM contact formatter can be passed a CRM field name to display.')
								),
					'submit_button' =>
					array('#type' => 'submit',
								'#value' => $submit_value),
					'delete_button' =>
					array('#type' => 'submit',
								'#value' => $delete_value),
					'copy_button' =>
					array('#type' => 'submit',
								'#value' => $copy_value)
					);
	$output .= drupal_get_form('query_export_formatter_form', $form);
	return $output;

}



/**
 * Editor/Deleter form for default parameters
 *
 */
function query_export_edit_default_params_form($nid, $param_name = NULL){
	$submit_value = t('Submit');
	$delete_value = t('Delete');
	$copy_value = t('Copy');
	
	if (!$nid) 
	  drupal_not_found();
  $qdef = node_load($nid);
  if (!$qdef)
    drupal_not_found();
  //See if we have previous values
  $old = query_export_fetch_default_param($nid, $param_name);
  
	$output = t("<p>Use this form to declare parameters for your custom
queries and to set default values for them.  You can override the defaults
when you run a report by using the pattern of key1/val1/key2/val2.... in
your query string.</p><p>A param 'key1' will appear in custom SQL as
%key1%, and will get substituted for the corresponding value when the
report is run.
</p>
");
	$form['qe_values'] = 
		array('nid' =>
					array('#type' => 'value',
								'#value' => $nid));
   $form['general_info'] =
     array('#type' => 'fieldset',
           '#title' => t('For Field'));
   $form['general_info']['which_nid'] =
     array('#type' => 'item',
           '#title' => t('Query Title'),
           '#value' => $qdef->title);
	//We may be called for a new field or to edit an existing one
	if (!$param_name)
		$form['general_info']['param_name'] =
			array('#type' => 'textfield',
						'#title' => t('Parameter Name'),
						'#description' => t('A parameter to pass in to a query such as a start or end date, or a contact or group id.  Should be a variable type name, i.e., just alphanumeric with an underscore if it will improve readability.'));
	else{
		$form['qe_values']['param_name'] =
			array('#type' => 'value',
						'#value' => $param_name);
   $form['general_info']['which_param'] =
     array('#type' => 'item',
           '#title' => t('Parameter Name'),
           '#value' => $param_name);
		
	}
	$form['parameter_form'] =
		array('default_value' =>
					array('#type' => 'textfield',
								'#size' => 40,
								'#default_value' => $old['default_value'] ? $old['default_value'] : NULL,
								'#title' => t('Default Value For Parameter'),
								'#description' =>
								t('If this parameter is not passed in a query string, what value should it have?')
								),
					'description' =>
					array('#type' => 'textarea',
								'#size' => 40,
								'#default_value' => $old['description'] ? $old['description'] : NULL,
								'#title' => t('Description'),
								'#description' =>
								t('Explain what this parameter signifies, and give a brief description of what values for it are valid')
								),
					'submit_button' =>
					array('#type' => 'submit',
								'#value' => $submit_value),
					'delete_button' =>
					array('#type' => 'submit',
								'#value' => $delete_value)
					);
	$output .= drupal_get_form('query_export_params_form', $form);
	return $output;

}



/**
 * Form processor callbacks
 *
 */


function query_export_formatter_form_validate($form_id, $form_values){
	//do not really need to do anything here, but will use it for debugging
	//error_log("$form_id validate was called");
	if (!isset($form_values['field_name'])) {
	  form_set_error('field_name', t('Please select a field name'));
	}
}


function query_export_formatter_form_submit($form_id, $form_values){
	//error_log("$form_id submit was called");
	if ($op == t('Delete')) {
	  //Nuke it
	  $sql = "DELETE FROM {query_export_data_handlers} WHERE nid = %d AND field_name = '%s'";
	  db_query($sql, $form_values['nid'], $form_values['field_name']);
	}
	else {
	  //Treat it as a submit
	  $curr_data = query_export_fetch_field_def($form_values['nid'], $form_values['field_name']);
	  if ($curr_data) {
	    $sql = "UPDATE {query_export_data_handlers} SET handler = '%s', extra = '%s'  " .
	           "WHERE nid = %d AND field_name = '%s'";
	    db_query($sql, $form_values['handler'], $form_values['extra'], 
	                   $form_values['nid'], $form_values['field_name']);
	  }
	  else {
	    $sql = "INSERT INTO {query_export_data_handlers} (handler, extra, nid, field_name) " . 
	           "VALUES('%s','%s',%d,'%s')";
	    db_query($sql, $form_values['handler'], $form_values['extra'], 
	                   $form_values['nid'], $form_values['field_name']);
	           
	  }
	}
  drupal_goto('node/' . $form_values['nid']);
}



/**
 * Form hooks for param form
 *
 */
function query_export_params_form_validate($form_id, $form_values){
	//do not really need to do anything here, but will use it for debugging
	//error_log("$form_id validate was called");
	if (!isset($form_values['param_name'])) {
	  form_set_error('param_name', t('Please select a parameter name.'));
	}
}


function query_export_params_form_submit($form_id, $form_values){
	//error_log("$form_id submit was called");
	if ($op == t('Delete')) {
	  //Nuke it
	  $sql = "DELETE FROM {query_export_param_defaults} WHERE nid = %d AND param_name = '%s'";
	  db_query($sql, $form_values['nid'], $form_values['param_name']);
	}
	else {
	  //Treat it as a submit
	  $curr_data = query_export_fetch_field_def($form_values['nid'], $form_values['param_name']);
	  if ($curr_data) {
	    $sql = "UPDATE {query_export_param_defaults} SET default_value = '%s', description = '%s'  " .
	           "WHERE nid = %d AND param_name = '%s'";
	    db_query($sql, $form_values['default_value'], $form_values['description'], 
	                   $form_values['nid'], $form_values['param_name']);
	  }
	  else {
	    $sql = "INSERT INTO {query_export_param_defaults} (default_value, description, nid, param_name) " . 
	           "VALUES('%s','%s',%d,'%s')";
	    db_query($sql, $form_values['default_value'], $form_values['description'], 
	                   $form_values['nid'], $form_values['param_name']);
	           
	  }
	}
  drupal_goto('node/' . $form_values['nid']);
}


/**
 * Fetch a field def if it exists
 *
 * @param int $nid
 * @param string $field_name
 */
function query_export_fetch_field_def($nid, $field_name){
  $sql = "SELECT * FROM {query_export_data_handlers} WHERE nid = %d AND field_name = '%s'";
  if ($nid and $field_name) {
    return db_fetch_array(db_query($sql, $nid, $field_name));
  }
  return NULL;
}


function query_export_fetch_default_param($nid, $param_name = NULL){
	if (!$param_name) {
		//Fetch them all
		$sql = "SELECT * FROM {query_export_param_defaults} WHERE nid = %d";
		$params = array();
		$rslt = db_query($sql, $nid);
		while ($row = db_fetch_array($rslt)) {
			$params['param_name'] = $row;
		}
		return $params;
	}
	//Otherwise, get just one.
  $sql = "SELECT * FROM {query_export_param_defaults} WHERE nid = %d AND param_name = '%s'";
  if ($nid and $param_name) {
    return db_fetch_array(db_query($sql, $nid, $param_name));
  }
  return NULL;
}


/**
 * Generated views def code for our field display embedded view.
 *
 * @param unknown_type $views
 */
function query_export_field_display_view(&$views){
  //Generated View Export code
  $view = new stdClass();
  $view->name = 'query_export_field_defs';
  $view->description = 'Embeddable View of a field list table';
  $view->access = array (
  0 => '3',
  );
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = 'Test of Embeddable Field View';
  $view->page_header = '';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = '';
  $view->page_empty_format = '1';
  $view->page_type = 'table';
  $view->url = 'reporting/fields_embeddable';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '10';
  $view->sort = array (
  );
  $view->argument = array (
    array (
      'type' => 'nid',
      'argdefault' => '1',
      'title' => '',
      'options' => '',
    ),
  );
  $view->field = array (
    array (
      'tablename' => 'query_export_formatters',
      'field' => 'field_name',
      'label' => 'Field Name',
      'handler' => 'query_export_formatter_field_link_handler',
    ),
    array (
      'tablename' => 'query_export_formatters',
      'field' => 'handler',
      'label' => 'Formatter',
    ),
    array (
      'tablename' => 'query_export_formatters',
      'field' => 'extra',
      'label' => 'Extra Data',
    ),
  );
  $view->filter = array (
    array (
      'tablename' => 'node',
      'field' => 'type',
      'operator' => 'OR',
      'options' => '',
      'value' => array (
  0 => 'query_export',
),
    ),
  );
  $view->exposed_filter = array (
  );
  $view->requires = array(query_export_formatters, node);
  $views[$view->name] = $view;

}


/**
 * Generated view for parameter defaults
 *
 */
function query_export_param_display_view(&$views){
  $view = new stdClass();
  $view->name = 'query_export_param_embed';
  $view->description = 'Embeddable view for default parameter values';
  $view->access = array (
  0 => '3',
);
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = '';
  $view->page_header = '';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = '';
  $view->page_empty_format = '1';
  $view->page_type = 'table';
  $view->url = 'reporting/param_test';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '4';
  $view->sort = array (
  );
  $view->argument = array (
    array (
      'type' => 'nid',
      'argdefault' => '1',
      'title' => '',
      'options' => '',
    ),
  );
  $view->field = array (
    array (
      'tablename' => 'query_export_parameters',
      'field' => 'param_name',
      'label' => 'Param Name',
      'handler' => 'query_export_param_link_handler',
      'sortable' => '1',
      'defaultsort' => 'ASC',
    ),
    array (
      'tablename' => 'query_export_parameters',
      'field' => 'default_value',
      'label' => 'Default Value',
    ),
    array (
      'tablename' => 'query_export_parameters',
      'field' => 'description',
      'label' => '',
    ),
  );
  $view->filter = array (
  );
  $view->exposed_filter = array (
  );
  $view->requires = array(query_export_parameters);
  $views[$view->name] = $view;
	return $views;

}




?>