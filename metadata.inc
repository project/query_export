<?php
/**
 *
 * Abstraction layer for meta data.
 *
 *
 */


/*
  General notes: this file will load and cache meta data definitions.
  Different data formats get abstracted here.

*/


/**
 * Turn a query designator into something more
 * user friendly
 */
function query_export_query_ui_name($qname){
  list($namespace, $name) = explode('::', $qname);
  switch($namespace){
  case 'custom':
    list($prefix, $nid) = explode('_', $name);
    if ($nid and is_numeric($nid)) {
      $node = node_load($nid);
      if ($node->type == 'query_export') {
        return $node->title;
      }
    }
    break;
  case 'civicrm':
    //a query is really a group
    //Note we use a dash and not an underscore here.
    list($prefix, $gid) = explode('-', $name);
    if (module_exist('civicrm') and module_exist('civinode')) {
      $group = civinode_get_group_by_id($gid);
      if (isset($group->title))
        return $group->title;
    }
    break;
  case 'views':
    if (module_exist('views')) {
      list($prefix, $vname) = explode('_', $name);
      //For now, just do the easy thing
      return $vname;
    }
    break;
  default:
    return $qname; //could not parse this
  }
  return $qname; 
}




/**
 * Get a meta data def for a drupal node type.
 * 
 * This function will maintain a cache.
 */

function query_export_get_type($engine, $qtype){
  static $cache = NULL;
  if(!is_array($cache)){
    $cache = array();
  }
  //Simple Cache Key
  $type = $engine . '::' . $qtype;
  if(!isset($cache[$type])){
    //Check for flexinode types
    if($engine == 'flexinode'){
      //Temp code
      list($prefix, $desc_id) = explode('-', $qtype);
      if($prefix == 'flexinode'){
        $mod_path = drupal_get_path('module', 'query_export');
        require_once $mod_path . '/flexinode.inc';
        //Note we are effectively doing negative caching here.
        $cache[$type] = query_export_get_flexinode_type($desc_id);
      }
    }
    elseif($engine == 'civicrm'){
      //Test code: we only allow a single profile,
      //and therefore, only a single type
      if (!module_exist('civinode')) {
        $cache[$type] = NULL;
        return NULL;
      }
      $mod_path = drupal_get_path('module', 'query_export');
      require_once $mod_path . '/civicrm.inc';
      global $user;
      if (is_numeric($qtype))
        $profile_id = $qtype;
      else
        $profile_id = civinode_get_default_profile_id($user->uid);
      $cache[$type] = query_export_get_civicrm_type($profile_id);
    }
    elseif($engine == 'views'){
      //Test code: we only allow a single profile,
      //and therefore, only a single type
      if (!function_exists('query_export_get_views_type')) {
        $mod_path = drupal_get_path('module', 'query_export');
        require_once $mod_path . '/views.inc';
      }
      $cache[$type] = query_export_get_views_type($qtype);
    }
    elseif($engine == 'custom') {
      //return array('private' => array());
      list($base, $nid) = explode('_', $qtype);
      query_export_load_field_definitions($nid);
      $cache[$type] = query_export_get_custom_meta_data($nid); 
    }
    else {
      //Not a legal name yet, so neg cache it
      //May want to return an XMLRPC error as well,
      //but maybe higher up.
      $cache[$type] = NULL;
    }
  }  
  return $cache[$type];
}


/**
 * Fetches the type object for a query
 *
 */
function query_export_get_query_type($qname){
  //TO DO: this code is simulating a query mapping.
  //this will be done later.  For now, A flexinode query
  //is of a whole type.  And a civicrm query is of a 
  //civicrm group linked to the default profile.
  list($namespace, $name) = explode('::', $qname);
  switch($namespace){
  case 'flexinode':
    //TEST: name *is* the type
    return query_export_get_type('flexinode', $name);
    break;
  case 'civicrm':
    //For now, all queries are of the same type,
    //since the profile determines type, and we
    //are not allowing the RPC code to specify this.
    return query_export_get_type('civicrm', 'dummy');
    break;
  case 'views':
    return query_export_get_type('views', $name);
    break;
  default:
    return NULL;
  }

}


/**
 * Output filter to remove things that xmlrpc_server probably
 * should, but does not.
 */
function query_export_filter_output($text){
	static $bad_chars;
	if (!$bad_chars) {
		$bad_chars[chr(11)] = ''; //vert tab
	}
	if ($text)
		return strtr($text, $bad_chars);
	else
		return $text;
}


/**
 * Render an array of IDs as an array of structs in canonical format
 *
 * This code needs some thought as to how to do this efficiently.  I'm
 * just going to so something simple for now
 *
 */
function query_export_get_record_array($engine, $qname,
                                       $query_nid = 0,
                                       $args=FALSE,
                                       $profile = 0,
                                       $start = 0, $num_recs = 0){
  //We assume we are getting an array of IDs, all of
  //a single type. These need not be NIDs (and for CRM,
  //probably will not be either).

  //TO DO: This code probably needs to filter by access right, but this
  //will be implemented later.
  error_log("Entering get record array for engine $engine and qname $qname");
  if ($engine == 'custom')
    $meta_data = array('engine' => 'custom');
  elseif ($engine != 'civicrm')
    $meta_data = query_export_get_type($engine, $qname);
  elseif ($profile != 0)
    $meta_data = query_export_get_type('civicrm', $profile);
  else
    $meta_data = query_export_get_type('civicrm', 'dummy');
    

  if(!$meta_data){
    //We don't know what to do with this.  Consider logging an error.
    error_log("Null metadata... bailing");
    return NULL;
  }

  //Check the engine and set up the call
  switch($meta_data["engine"]){
  case 'custom':
    return query_export_get_custom_records($qname, $query_nid, 
                                           $start, $num_recs);
    
  case 'flexinode':
    if (!module_exist('flexinode'))
      return array();
    return query_export_get_records_flexinode_type($meta_data, $qname, $query_nid,
                                                   $args, $start, $num_recs); 
  case 'civicrm':
    if (!module_exist('civinode'))
      return array();
    return query_export_get_civicrm_contact_array($meta_data, $qname, $query_nid, 
                                                  $args, $profile, $start, $num_recs);
  case 'views':
    return query_export_get_views_record_array($meta_data, $qname, $query_nid, 
                                               $args, $start, $num_recs);
  default:
    return array();
  }
}



/**
 * Custom record getter. A quick hack so we can run bits
 * of SQL
 *
 */
function query_export_get_custom_records($qname, $query_nid, $start, $num_recs){
  //For now, only custom form is "custom_NID"
  error_log("Entered get custom records");
  $returned = array();
  $match = array();
  if (!preg_match('/^custom_(\d+)$/', $qname, $match))
    return $returned;
  $nid = $match[1];
  //We ignore query_nid for now, since our nid IS our query_nid
  $sql = "SELECT query_sql FROM {query_export_reports} WHERE query_type = 'custom' AND query_sql IS NOT NULL AND nid = %d";
  $rslt = db_query($sql, $nid);
  if (!$rslt)
    return $returned;
  $custom_sql = db_result($rslt);
  if (!$custom_sql)
    return $returned;
  error_log("Have enough data to run custom query");
  //Now try to run this
  $rslt = db_query($custom_sql);
  //See if we have any formatters available
  query_export_load_field_definitions($nid);
  $formatters = query_export_fetch_formatter($nid);
  while ($row = db_fetch_array($rslt)) {
    if ($formatters) {
      foreach ($formatters as $fld => $data) {
        if (isset($row[$fld]))
          $row[$fld] = query_export_munge_field($nid, $fld, $data, $row[$fld]);
      }
    }
    $returned[] = $row;
  }
  error_log("About to return custom query result");
  return $returned;
    
}


?>
