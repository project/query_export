<?php
/**
 *
 * Metadata extractors for CiviCRM
 *
 * This file bridges CiviCRM and query_export
 *
 */

//$civinode_path = drupal_get_path('module', 'civinode');
//require_once $mod_path . "/civinode_utils.inc";


/**
 * 
 * Getter for a civicrm contact type.
 * 
 * @param $desc_id a descriptor for a Flexinode data type.  An int.
 * @return array meta data object 
 */


function query_export_get_civicrm_type($desc_id){
  //desc id for this type is really a profile_id
  $pid = $desc_id;

  $title = civinode_get_profile_title($pid);
  $fields = civinode_get_profile_field_list($pid);

  //The type exists, so build an object
  $meta_data = array(
		     'engine' => 'civicrm', //what we need to call
		     'name' => "$title", //use the profile name
		     'title' => "$title", //since name is user displayable.
         //description is not yet exposed to CRM API
		     'description' => "",
		     'fields' => query_export_get_crm_fields($desc_id) 
		     );
  return $meta_data;
  
}


function query_export_get_crm_fields($profile_id){
  //TO DO: do we need any action other than view?
  $meta_data = civinode_get_profile_metadata($profile_id, 'view');
  if(!$meta_data)
    return array(); //should likely log something as well
  $fields = civinode_get_profile_field_list($profile_id, 'view');
  $field_array = array();
  foreach($fields as $fld_name => $internal_name){
    $raw_data = civinode_get_field_info($profile_id, $internal_name, 'view');
    $field_def = array('name' => $fld_name,
		       'title' => $raw_data['title'],
		       'type' => 'textfield', //1.4 profiles don't help much
											 //'description' => $raw_data['help_post']
           'description' => ""
		       );
    $field_array[] = $field_def;
  }
  return $field_array;
}


/**
 * Record getter
 *
 */
function query_export_get_civicrm_contact_array($meta_data, $qname,
																								$query_nid,
																								$args, $profile,
																								$start, $num_recs){
  //These are just nodes, and our ids in this case are NIDs.
  $list = array();
  //TO DO: the only name type we support is by numbered group:
  list($base, $gid) = explode('-', $qname);
  if($base != 'group')
    return $list; //empty still
  $contacts = civinode_util_group_contacts($gid, $start, $num_recs);
  //TO DO: screen for access
  foreach($contacts as $cid){
    //TO DO: need to screen by profile
		$data = civinode_util_load_cdata($cid);
		//We need to filter a bit for bad chars
		$filtered = array();
		foreach ($data as $key => $val) {
			$filtered[$key] = query_export_filter_output($val);
		}
		$list[] = $filtered;
  }

  return $list;
}



/**
 * Private getters
 *
 */







?>